export interface IPlayer {
	readonly id: string
	readonly cellPhone: string
	readonly email: string
	readonly name: string
	readonly ranking: string
	readonly rankingPosition: number
	readonly urlPhotoPlayer: string
}
