export class ICreatePlayerDTO {
	readonly cellPhone: string
	readonly email: string
	readonly name: string
}
