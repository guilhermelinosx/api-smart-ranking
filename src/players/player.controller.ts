import { Body, Controller, Post } from '@nestjs/common'
import { ICreatePlayerDTO } from './DTOS/ICreatePlayerDTO'

@Controller('api/jogadores')
export class PlayerController {
	@Post()
	async criarAtualizarJogador(@Body() createPlayerDTO: ICreatePlayerDTO): Promise<string> {
		const { email } = createPlayerDTO
		return JSON.stringify({
			nome: 'João',
			email: `${email}`
		})
	}
}
